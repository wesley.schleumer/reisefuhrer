package li.ues.reisefuhrer.data

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.Await
import scala.slick.driver.JdbcDriver
import com.mchange.v2.c3p0.ComboPooledDataSource
import com.github.tototoshi.slick._
import com.github.tminglei.slickpg._
import Driver._
import scala.Enumeration
import scala.util.Try

object Context extends Driver.API {
  object JodaMapper extends GenericJodaSupport(JdbcDriver)

  // @todo(CRITICAL) redo with application.conf settings for the god sake
  val ds = new ComboPooledDataSource
  ds.setDriverClass("org.postgresql.Driver")
  ds.setUser("guiadolugar")
  ds.setPassword("jVc19iw5DMtP")
  ds.setJdbcUrl("jdbc:postgresql://localhost/guiadolugar")

  implicit val db = Database.forDataSource(ds)

  def process[R](action: DBIOAction[R, NoStream, Nothing]): R = {
    var execution = db.run(action)
    var y = new java.util.Date().getTime()
    execution.onSuccess {
      case x => { 
        println("Query executed in %d ms".format(new java.util.Date().getTime() - y))
      }
    }
    Await.result(execution, Duration.Inf)
  }

  def enumValueMapper(enum: Enumeration) = MappedColumnType.base[enum.Value, String](
    e => e.toString,
    s => Try(enum.withName(s)).getOrElse(throw new IllegalArgumentException(s"enumeration $s doesn't exist $enum [${enum.values.mkString(",")}]"))
  )
  
  def enumIdMapper(enum: Enumeration) = MappedColumnType.base[enum.Value, Int](
      e => e.id,
      i => enum.apply(i)
  )

}