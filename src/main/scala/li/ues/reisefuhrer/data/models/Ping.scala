package li.ues.reisefuhrer.data.models

import org.joda.time.DateTime

import spray.json._

object DescriptorType extends Enumeration {
  type DescriptorType = Value
  val ProgrammingLanguage = Value("programming_language")
}

import DescriptorType._

case class Descriptor(
  id: Option[Int],
  /* workaround for reserved word */ descriptorType: DescriptorType,
  name: String,
  meta: JsValue)

case class Ping(
  id: Option[Int],
  descriptorId: Int,
  senderId: Int,
  osId: Int,
  userId: Int,
  when: DateTime)