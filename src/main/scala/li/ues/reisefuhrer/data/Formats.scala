package li.ues.reisefuhrer.data

import spray.json._
import DefaultJsonProtocol._
import spray.http._
import spray.httpx.marshalling._
import spray.httpx._
import ContentTypes._
import scala.language.implicitConversions
import java.time.format._
import java.util._
import li.ues.reisefuhrer._
import scala.Enumeration

object Formats extends DefaultJsonProtocol with SprayJsonSupport {
  def jsonEnum[T <: Enumeration](enu: T) = new JsonFormat[T#Value] {
    def write(obj: T#Value) = JsString(obj.toString)

    def read(json: JsValue) = json match {
      case JsString(txt) => enu.withName(txt)
      case something => throw new DeserializationException(s"Expected a value from enum $enu instead of $something")
    }
  }


  implicit object DateJsonFormat extends RootJsonFormat[Date] {
    override def write(obj: Date) = JsNumber(obj.getTime())
    override def read(json: JsValue) : Date = json match {
      case JsNumber(s) => {
        new Date(s.toLong)
      }
      case _ => throw new DeserializationException("Error info you want here ...")
    }
  }

  implicit val resultErrorFormat = jsonFormat3(ResultError)
  implicit val resultMessageFormat = jsonFormat3(ResultMessage)
  implicit val emptyResultMessageFormat = jsonFormat2(EmptyResult)
  implicit val descriptorTypeFormat = jsonEnum(models.DescriptorType)
  implicit val descriptorFormat = jsonFormat4(models.Descriptor)
  implicit def resultOfFormat[A: JsonFormat] = jsonFormat(ResultOf.apply[A], "result", "errors", "messages")

  implicit def sprayJson[T : JsonFormat]
    (implicit writer: RootJsonWriter[T]) =
      Marshaller.delegate[T, String](`application/json`)
        { v => CompactPrinter(writer.write(v)) }
}