package li.ues.reisefuhrer

import spray.json._
import data.Formats._
import data.collections._
import data.Context._

object Main extends App {
  web.Server.boot()
}