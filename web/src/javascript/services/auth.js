import EE from 'events';
import { Promise } from 'bluebird';
import axios from 'axios';

// since it's a service, it's a singleton object
var Auth = {
  ee: new EE(),
  user: null,
  login: (user, password) => axios.post('/auth', { user, password }),
  onLogin: (fn) => Auth.ee.on('login', fn),
  onLogout: (fn) => Auth.ee.on('logout', fn),
  isLogged: () => !!Auth.user,
}

export default Auth;
