import React from 'react';
import { Container } from '../components';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.displayName = 'Login';
    }
    render() {
        return <div style={{ marginTop: '200px' }}>{this.props.children}</div>;
    }
}

export default Login;
