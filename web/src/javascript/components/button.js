import React from 'react';

class Button extends React.Component {
  constructor(props) {
    super(props);
    this.displayName = 'Button';
  }
  render() {
    var props = {...this.props};
    var submit = props.submit || false;
    var className = ["btn", this.props.type];


    props.className = className.join(' ');

    return <button type={submit ? "submit" : "button"} {...props}>{this.props.text}</button>;
  }
}

Button.Default = "btn-default";
Button.Primary = "btn-primary";
Button.Success = "btn-success";
Button.Info = "btn-info";
Button.Danger = "btn-danger";
Button.Warning = "btn-warning";
Button.Link = "btn-link";

export default Button;
