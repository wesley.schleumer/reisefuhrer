import React from 'react';

import Form from './form';

class TextInput extends React.Component {
  constructor(props) {
    super(props);
    this.displayName = 'TextInput';
    this.state = {
      value: null
    }
  }
  onChange(evt) {
    this.setState({ value: evt.target.value });
  }
  getValue() {
    return this.state.value;
  }
  componentWillMount() {
    this.state.value = this.props.value || null;
  }
  render() {
    // @todo helper
    var label = this.props.label || null;
    var placeholder = this.props.placeholder || label;

    // we love shortcut
    return (
      <Form.Group>
        {label && <label>{label}</label>}
        <input type="text" className="form-control" placeholder={placeholder} onChange={this.onChange.bind(this)} value={this.state.value} />
      </Form.Group>
    );
  }
}

export default TextInput;
