import React from 'react';
import EventEmitter from 'events';

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.displayName = 'Form';
    this.ee = new EventEmitter();
    this.created = new Date();
  }
  render() {
    if(Array.isArray(this.props.children)) {
      React.Children.forEach(this.props.children, function(element) {
        console.log(element);
      });
    }
    return (
      <form {...this.props}>
        {this.props.children}
      </form>
    );
  }
}


Form.Group = require('./group');
Form.Footer = require('./footer');

export default Form;