import React from 'react';
  
class Row extends React.Component {
  constructor(props) {
    super(props);
    this.displayName = 'Row';
  }
  render() {
    // @todo coding style
    var className = [this.props.fluid ? "row-fluid" : "row"];

    if (this.props.className) {
      className.push(this.props.className);
    }

    return <div {...this.props}
      className={className.join(' ')}
      style={{ ...this.props.style }}>
      {this.props.children}
    </div>;
  }
}

export default Row;
