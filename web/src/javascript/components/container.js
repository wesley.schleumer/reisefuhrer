import React from 'react';
  
class Container extends React.Component {
  constructor(props) {
    super(props);
    this.displayName = 'Container';
  }
  render() {
    // @todo coding style
    var className = [this.props.fluid ? "container-fluid" : "container"];

    if (this.props.className) {
      className.push(this.props.className);
    }

    return (
      <div {...this.props}
        className={className.join(' ')}
        style={{ ...this.props.style }}>
          {this.props.children}
      </div>
    );
  }
}

export default Container;
